import 'package:flutter/material.dart';
import 'package:profile_app/pages/sports_page.dart';
import 'package:profile_app/widgets/custom_app_bar.dart';
import 'package:profile_app/widgets/custom_drawer.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ProfilePageState();
  }
}

class _ProfilePageState extends State<ProfilePage> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _ageController = TextEditingController();
  final TextEditingController _jobController = TextEditingController();

  String _userName = "Mohammad Hussein Hamed";
  String _userJob = "Student at AIU university";
  String _userAge = "21";
  @override
  void initState() {
    _nameController.text = _userName;
    _ageController.text = _userAge;
    _jobController.text = _userJob;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _buildPage(context);
  }

  Widget _buildPage(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        titleText: "Profile",
        actionsList: [
          IconButton(
            onPressed: () {
              _editInfo();
            },
            icon: Icon(
              Icons.edit_square,
              color: Colors.grey.shade300,
            ),
          )
        ],
      ),
      drawer: CustomDrawer(
        context: context,
        runtimeType: runtimeType,
      ),
      body: SizedBox.expand(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 180,
                decoration: const BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child: Text(
                        _userName,
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Container(
                      height: 90,
                      width: 90,
                      clipBehavior: Clip.hardEdge,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            "assets/images/Dragon_of_the_east.jpg",
                          ),
                        ),
                        shape: BoxShape.circle,
                        border: Border.fromBorderSide(
                          BorderSide(
                            strokeAlign: BorderSide.strokeAlignOutside,
                            color: Colors.white,
                            width: 2,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(height: 25),
                    _pageItem(
                      icon: Icons.calendar_month,
                      title: "Age",
                      content: _userAge,
                    ),
                    const SizedBox(height: 20),
                    _pageItem(
                      icon: Icons.work,
                      title: "Job",
                      content: _userJob,
                    ),
                    const SizedBox(height: 50),
                    _sportsButton(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container _pageItem({
    required IconData icon,
    required String title,
    required String content,
  }) {
    return Container(
      height: 60,
      decoration: BoxDecoration(
        color: Colors.grey.shade800,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(width: 15),
          Icon(
            size: 25,
            icon,
          ),
          const SizedBox(width: 20),
          Text(
            title,
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(width: 30),
          Text(
            content,
            style: const TextStyle(fontSize: 14),
          )
        ],
      ),
    );
  }

  Widget _sportsButton() {
    return MaterialButton(
      onPressed: () => Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => const SportsPage(),
        ),
      ),
      color: Colors.blue,
      padding: const EdgeInsets.symmetric(
        horizontal: 30,
        vertical: 20,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: const Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.sports_martial_arts,
            color: Colors.white,
            size: 20,
          ),
          SizedBox(width: 15),
          Text(
            "Favorite Sports",
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  _editInfo() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text(
            "Edit",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 40),
              _textFieldWidget(
                labelText: "Name",
                textController: _nameController,
              ),
              const SizedBox(height: 20),
              _textFieldWidget(
                labelText: "Age",
                textController: _ageController,
              ),
              const SizedBox(height: 20),
              _textFieldWidget(
                labelText: "Job",
                textController: _jobController,
              ),
              const SizedBox(height: 20),
            ],
          ),
          actionsAlignment: MainAxisAlignment.spaceEvenly,
          actions: [
            MaterialButton(
              onPressed: () {
                setState(() {
                  _userName = _nameController.text;
                  _userAge = _ageController.text;
                  _userJob = _jobController.text;
                });
                Navigator.of(context).pop();
              },
              color: Colors.green,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: const Text(
                "Save",
                style: TextStyle(
                  fontSize: 12,
                ),
              ),
            ),
            MaterialButton(
              onPressed: () {
                _nameController.text = _userName;
                _ageController.text = _userAge;
                _jobController.text = _userJob;
                Navigator.of(context).pop();
              },
              color: Colors.blue,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: const Text(
                "Cancel",
                style: TextStyle(
                  fontSize: 12,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  TextField _textFieldWidget({
    required String labelText,
    required TextEditingController textController,
  }) {
    return TextField(
      autofocus: false,
      controller: textController,
      decoration: InputDecoration(
        label: Text(
          labelText,
          style: const TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 18,
          ),
        ),
        filled: true,
        fillColor: Colors.grey.shade900,
        focusColor: Colors.grey.shade900,
        hoverColor: Colors.grey.shade900,
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      style: const TextStyle(
        fontSize: 14,
      ),
    );
  }
}
