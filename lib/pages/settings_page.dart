import 'package:flutter/material.dart';
import 'package:profile_app/widgets/custom_app_bar.dart';
import 'package:profile_app/widgets/custom_drawer.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return _buildPage(context);
  }

  Widget _buildPage(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        titleText: "Settings",
      ),
      drawer: CustomDrawer(
        runtimeType: runtimeType,
        context: context,
      ),
    );
  }
}
