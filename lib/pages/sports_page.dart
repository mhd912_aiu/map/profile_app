import 'package:flutter/material.dart';
import 'package:profile_app/widgets/custom_app_bar.dart';
import 'package:profile_app/widgets/custom_drawer.dart';

class SportsPage extends StatelessWidget {
  const SportsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return _buildPage(context);
  }

  Widget _buildPage(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        titleText: "Favorite Sports",
      ),
      drawer: CustomDrawer(
        runtimeType: runtimeType,
        context: context,
      ),
      body: SizedBox.expand(
        child: ListView(
          children: [
            _sportContainer(
              sportName: "Football",
              sportDescription:
                  "Football is one of the most famous sports in the world. It has a huge fan base because of how much entertaining it can be to watch or play in person.",
              sportImage: "assets/images/football.jpeg",
            ),
            _sportContainer(
              sportName: "eSports",
              sportDescription:
                  "eSports, short for electronic sports, are essentially organized competitions using video games. Professional gamers, either individually or in teams, face off in multiplayer matches for glory and prize money. These competitions are often live-streamed for a large audience, just like traditional sports. eSports have become particularly popular in recent years, with games like League of Legends and Dota 2 attracting millions of viewers.",
              sportImage: "assets/images/esport.jpg",
            ),
            _sportContainer(
              sportName: "Chess",
              sportDescription:
                  "Chess can be considered a sport, even though it doesn't involve intense physical activity.  It requires mental stamina, strategic thinking, and the ability to manage pressure during competition.  Just like in other sports, chess players train, compete in tournaments, and vie for titles. The International Olympic Committee even recognizes chess as a sport, though it's not currently part of the Olympic Games.",
              sportImage: "assets/images/chess.jpg",
            ),
          ],
        ),
      ),
    );
  }

  Container _sportContainer({
    required String sportName,
    required String sportDescription,
    required String sportImage,
  }) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      margin: const EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 25,
      ),
      decoration: BoxDecoration(
        color: Colors.grey.shade800,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 10),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              sportName,
              style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w700,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
          const SizedBox(height: 30),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              sportDescription,
              style: const TextStyle(
                fontSize: 14,
              ),
            ),
          ),
          const SizedBox(height: 20),
          Container(
            height: 200,
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(
                  sportImage,
                ),
              ),
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
